﻿namespace NHEF.Demo
{
    using System.IO;
    using System.Linq;
    using NHEF.Demo.Mapping;
    using NHEF.Demo.Model;
    using NHibernate;
    using NHibernate.Cfg;
    using NHibernate.Dialect;
    using NHibernate.Driver;
    using NHibernate.Mapping.ByCode;
    using NUnit.Framework;

    [TestFixture]
    public class Example
    {
        #region Setup/Teardown

        [SetUp]
        public void SetUp()
        {
            var configure = new Configuration();
            configure.DataBaseIntegration(
                x =>
                    {
                        x.Dialect<SQLiteDialect>();
                        x.Driver<SQLite20Driver>();
                        x.ConnectionString = "Data Source=db.sqlite;";
                        x.LogFormattedSql = true;
                        x.LogSqlInConsole = true;
                        x.SchemaAction = SchemaAutoAction.Recreate;
                    });

            var mapper = new ModelMapper();
            mapper.AddMapping<CustomerMapping>();
            configure.AddMapping(mapper.CompileMappingForAllExplicitlyAddedEntities());

            sessionFactory = configure.BuildSessionFactory();
        }

        [TearDown]
        public void TearDown()
        {
            File.Delete("db.sqlite");
        }

        #endregion

        private ISessionFactory sessionFactory;

        [Test]
        public void Can_add_remove_change_customer()
        {
            // add customer
            using (var context = new DemoContext(sessionFactory))
            {
                var customer = new Customer
                                   {
                                       Name = "customer 1"
                                   };

                context.Customers.AddObject(customer);
                context.SaveAllChanges();
            }

            using (var context = new DemoContext(sessionFactory))
            {
                Assert.That(context.Customers.Count(), Is.EqualTo(1));
                Customer customer = context.Customers.First();

                Assert.That(customer.Name, Is.EqualTo("customer 1"));
            }

            // update
            using (var context = new DemoContext(sessionFactory))
            {
                Customer customer = context.Customers.First();
                customer.Name = "changed";

                context.SaveAllChanges();
            }

            using (var context = new DemoContext(sessionFactory))
            {
                Assert.That(context.Customers.Count(), Is.EqualTo(1));
                Customer customer = context.Customers.First();

                Assert.That(customer.Name, Is.EqualTo("changed"));
            }

            // delete
            using (var context = new DemoContext(sessionFactory))
            {
                Customer customer = context.Customers.First();
                context.Customers.DeleteObject(customer);

                context.SaveAllChanges();
            }

            using (var context = new DemoContext(sessionFactory))
            {
                Assert.That(context.Customers.Count(), Is.EqualTo(0));
            }
        }
    }
}