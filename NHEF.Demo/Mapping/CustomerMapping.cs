﻿namespace NHEF.Demo.Mapping
{
    using NHEF.Demo.Model;
    using NHibernate.Mapping.ByCode;
    using NHibernate.Mapping.ByCode.Conformist;

    public class CustomerMapping : ClassMapping<Customer>
    {
         public CustomerMapping()
         {
             Id(x => x.Id, x => x.Generator(new IdentityGeneratorDef()));
             Property(x => x.Name);
         }
    }
}