﻿namespace NHEF.Demo
{
    using System.Data.Objects;
    using NHEF.Demo.Model;
    using NHibernate;
    using ObjectContext = NHEF.ObjectContext;

    public class DemoContext : ObjectContext
    {
        public DemoContext(ISessionFactory sessionFactory)
            : base(sessionFactory)
        {
        }

        public IObjectSet<Customer> Customers { get; private set; }
    }
}