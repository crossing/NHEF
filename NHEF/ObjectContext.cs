﻿namespace NHEF
{
    using System;
    using System.Data.Objects;
    using System.Linq;
    using System.Reflection;
    using NHibernate;

    public abstract class ObjectContext : IDisposable
    {
        private readonly ISession session;

        protected ObjectContext(ISessionFactory sessionFactory)
        {
            if (sessionFactory == null)
            {
                throw new ArgumentNullException("sessionFactory");
            }

            session = sessionFactory.OpenSession();

            SetUpObjectSets();
        }

        public void Dispose()
        {
            session.Dispose();
        }

        public void SaveAllChanges()
        {
            using (var tx = session.BeginTransaction())
            {
                tx.Commit();
            }
        }

        private void SetUpObjectSets()
        {
            var properties = GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public);

            foreach (var property in properties.Where(IsEntityCollection))
            {
                var entityType = property.PropertyType.GetGenericArguments()[0];
                var objectSet = Activator.CreateInstance(
                    typeof (ObjectSet<>).MakeGenericType(entityType),
                    session);
                property.SetValue(this, objectSet, null);
            }
        }

        private bool IsEntityCollection(PropertyInfo property)
        {
            if (property.GetSetMethod(true) == null)
            {
                return false;
            }

            if (property.GetGetMethod() == null)
            {
                return false;
            }

            if (!property.PropertyType.IsGenericType)
            {
                return false;
            }

            if (property.PropertyType.GetGenericTypeDefinition() != typeof (IObjectSet<>))
            {
                return false;
            }

            var entityType = property.PropertyType.GetGenericArguments()[0];
            return session.SessionFactory.GetClassMetadata(entityType) != null;
        }
    }
}