﻿namespace NHEF
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data.Objects;
    using System.Linq;
    using System.Linq.Expressions;
    using NHibernate;
    using NHibernate.Linq;

    public class ObjectSet<TEntity> : IObjectSet<TEntity> where TEntity : class
    {
        private readonly ISession session;

        public ObjectSet(ISession session)
        {
            if (session == null)
            {
                throw new ArgumentNullException("session");
            }

            this.session = session;
        }

        #region IObject Set<TEntity> Members

        public IEnumerator<TEntity> GetEnumerator()
        {
            return session.Query<TEntity>().GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public Expression Expression
        {
            get { return session.Query<TEntity>().Expression; }
        }

        public Type ElementType
        {
            get { return session.Query<TEntity>().ElementType; }
        }

        public IQueryProvider Provider
        {
            get { return session.Query<TEntity>().Provider; }
        }

        public void AddObject(TEntity entity)
        {
            session.Save(entity);
        }

        public void Attach(TEntity entity)
        {
            session.SaveOrUpdate(entity);
        }

        public void DeleteObject(TEntity entity)
        {
            session.Delete(entity);
        }

        public void Detach(TEntity entity)
        {
            session.Evict(entity);
        }

        #endregion
    }
}